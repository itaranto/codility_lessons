package main

import (
	"testing"
)

var tapeEquilibriumTests = []struct {
	name string
	a    []int
	want int
}{
	{
		name: "01",
		a:    []int{3, 1, 2, 4, 3},
		want: 1,
	},
	{
		name: "02",
		a:    []int{2, 0, 5, 6, -1, -5},
		want: 3,
	},
	{
		name: "03",
		a:    []int{-8, 1, -3, -5, 4, -1},
		want: 2,
	},
	{
		name: "04",
		a:    []int{1, 2},
		want: 1,
	},
	{
		name: "04",
		a:    []int{2, 2},
		want: 0,
	},
}

func TestTapeEquilibriumCuadratic(t *testing.T) {
	for _, test := range tapeEquilibriumTests {
		t.Run(test.name, func(t *testing.T) {
			got := tapeEquilibriumCuadratic(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestTapeEquilibriumLinear(t *testing.T) {
	for _, test := range tapeEquilibriumTests {
		t.Run(test.name, func(t *testing.T) {
			got := tapeEquilibriumLinear(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
