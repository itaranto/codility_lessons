package main

import (
	"slices"
	"testing"
)

var maxCountersTests = []struct {
	name string
	n    int
	a    []int
	want []int
}{
	{
		name: "Example",
		n:    5,
		a:    []int{3, 4, 4, 6, 1, 4, 4},
		want: []int{3, 2, 2, 4, 2},
	},
	{
		name: "AllMaxCounters",
		n:    5,
		a:    []int{6, 6, 6, 6, 6, 6, 6},
		want: []int{0, 0, 0, 0, 0},
	},
	{
		name: "NoMaxCounter",
		n:    5,
		a:    []int{1, 2, 3, 4, 5, 5, 5},
		want: []int{1, 1, 1, 1, 3},
	},
}

func TestMaxCountersNaive(t *testing.T) {
	for _, test := range maxCountersTests {
		t.Run(test.name, func(t *testing.T) {
			got := maxCountersNaive(test.n, test.a)
			if !slices.Equal(got, test.want) {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestMaxCountersLinear(t *testing.T) {
	for _, test := range maxCountersTests {
		t.Run(test.name, func(t *testing.T) {
			got := maxCountersLinear(test.n, test.a)
			if !slices.Equal(got, test.want) {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
